// components/bkp-icon/bkp-icon.js
Component({
    externalClasses: ['icon-class'],
    /**
     * 组件的属性列表
     */
    properties: {
        name:{
            type:String,
            value:'icon-user'
        }
    },

    /**
     * 组件的初始数据
     */
    data: {

    },

    /**
     * 组件的方法列表
     */
    methods: {

    }
})
