import dayjs from "dayjs";
import calendar from "../../../../static/js/calendar";

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    beginDay: {
      type: String,
      value: "",
    },
    month: {
      type: String,
      value: "1",
    },
    value: {
      type: String,
      value: "",
    },
  },

  //生命周期
  lifetimes: {
    attached() {
      // console.log('456')
      // this.initDay()
    },
  },
  observers: {
    beginDay: function () {
      if (this.properties.beginDay !== "") {
        this.initDay();
      }
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    dayList: [],
  },
  /**
   * 组件的方法列表
   */
  methods: {
    initDay() {
      let dayList = [];
      let currentMonth = +this.properties.month;
      for (let i = 0; i < 42; i++) {
        let day = dayjs(this.properties.beginDay)
          .add(i, "day")
          .format("YYYY-MM-DD");
        let month = +dayjs(day).format("MM");
        let { IMonthCn = "", IDayCn = "" } = calendar.solar2lunar(
          dayjs(day).format("YYYY"),
          dayjs(day).format("MM"),
          dayjs(day).format("DD")
        );
        let item = {
          today: day === dayjs().format("YYYY-MM-DD"),
          isCurrentMonth: currentMonth === month,
          solarDay: +dayjs(day).format("DD"),
          lunarDay: IDayCn,
          date: day,
          lunarDate: IMonthCn + IDayCn,
        };
        dayList.push(item);
      }
      this.setData({
        dayList: dayList,
      });
    },
    onClickDay(e) {
      let detail = {
        dayInfo: e.currentTarget.dataset.day,
      };
      this.triggerEvent("click-day", detail);
    },
  },
});
