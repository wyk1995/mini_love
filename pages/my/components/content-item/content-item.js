// pages/my/components/content-item/content-item.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        action:{
            type:String,
            value:''
        },
        iconName:{
            type:String,
            value:'iconfont-boy_girl'
        },
        name:{
            type:String,
            value:'另一半'
        }
    },

    /**
     * 组件的初始数据
     */
    data: {

    },

    /**
     * 组件的方法列表
     */
    methods: {
        onClickAction(){
            let detail={
                action:this.properties.action
            }
            this.triggerEvent('click-action',detail)
        }
    }
})
