import {throttle} from '../../../../utils/throttle_debounce'

Component({
    /**
     * 组件的属性列表
     */
    properties: {
        userInfo:{
            type:Object,
            value(){
                return{
                    userSysID:-1
                }
            }
        }
    },
    data: {

    },
    methods: {
        onClickLogin:throttle(function(){
            this.triggerEvent('click-login')
        },2000,{'trailing': false})
    }
})
