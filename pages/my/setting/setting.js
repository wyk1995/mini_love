const app = getApp();
Page({
    data: {
        app:app,
    },
    onClickLogout(){
        wx.removeStorageSync('userInfo');
        wx.removeStorageSync('signData');
        app.globalData.userInfo={
            userSysID:-1,
            nickName:'',
            avatarUrl:''
        };
        app.globalData.openId='';
        wx.navigateBack({
            delta:1
        })
    },
    onClickEditUser(){
        let actionType='edit'
        wx.navigateTo({
          url: '../register/register?actionType='+actionType,
        })
    }
})