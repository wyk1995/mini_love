const app = getApp();
import dayjs from "dayjs";
import { getUserOpenID, userSignIn,getUserLimit,getOtherHalfInfoBySysID } from "../../api/my_v1";
import { throttle } from "../../utils/throttle_debounce";
import Notify from "@vant/weapp/notify/notify";
Page({
  data: {
    app: app,
    sign: {
      isSign: 0,
      couplesMoney: 0,
      continueSign: 0,
      date: "",
    },
    otherInfo: {
      isSingle: 0,
      loveDate: "",
      avatarUrl: "",
      nickName: "",
    },
    audit:false,
    title:'随心所欲'
  },
  onShow() {
    let signData = wx.getStorageSync("signData")
      ? JSON.parse(wx.getStorageSync("signData"))
      : { date: "" };
    let otherHalfInfo = wx.getStorageSync("otherHalfInfo");
    let { date = "" } = signData;
    let today = dayjs().format("YYYY-MM-DD");
    if (date === today && otherHalfInfo) {
      let otherInfo = JSON.parse(otherHalfInfo);
      this.setData({
        sign: signData,
        otherInfo: otherInfo,
      });
    } else if (app.globalData.userInfo.userSysID !== -1) {
      this.GetOtherHalfInfoBySysID(app.globalData.userInfo.userSysID);
    } else {
      let sign = {
        isSign: 0,
        couplesMoney: 0,
        continueSign: 0,
        date: "",
      };
      this.setData({
        sign: sign,
      });
    }
    this.GetUserLimit()
  },
  //登录/注册
  onClickLogin(){
      let that=this;
    wx.login({
        success(res) {
            // console.log(res)
            let JsCode=res.code;
            let reqData={
                JsCode:JsCode
            }
            getUserOpenID(reqData)
                .then(({code,data,msg})=>{
                    if(code===200){
                        let {ExistUser, OpenID = "", UserInfo = {}}=data;
                        if(ExistUser){
                            app.globalData.openId = OpenID;
                            const {SysID=-1,NickName,AvatarUrl}=UserInfo;
                            let newUserInfo = {
                                userSysID: +SysID,
                                nickName: NickName,
                                avatarUrl: AvatarUrl,
                            };
                            wx.setStorage({
                                key: "userInfo",
                                data: JSON.stringify(newUserInfo),
                            });
                            wx.setStorage({
                                key: "openId",
                                data: OpenID,
                            });
                            app.globalData.userInfo= newUserInfo,
                            that.GetOtherHalfInfoBySysID(+SysID);
                            that.setData({
                                app:app
                            })
                        }
                        else{
                            //不存在用户需要去注册
                            let actionType='register'
                            wx.setStorage({
                                key: "openId",
                                data: OpenID,
                            });
                            app.globalData.openId = OpenID;
                            wx.navigateTo({
                                url: "./register/register?actionType="+actionType,
                            });
                        }
                    }
                    else{
                        Notify({ type: "danger", message: msg });
                    }
                })
                .catch((e)=>{
                    Notify({ type: "danger", message: "遇到了未知的错误,暂时无法登录/注册!" });
                })
        },
        fail(err) {
            Notify({ type: "danger", message: "遇到了未知的错误,暂时无法登录/注册!" });
        },
    });
  },
  //打卡
  onClickSign: throttle(
    function () {
      let reqData = {
        UserSysID: app.globalData.userInfo.userSysID,
      };
      userSignIn(reqData)
        .then(({ code, data }) => {
            if (code === 200) {
                let { CouplesMoney = 0, ContinueSign = 0 } = data;
                let sign = {
                    isSign: 1,
                    couplesMoney: CouplesMoney,
                    continueSign: ContinueSign,
                    date: dayjs().format("YYYY-MM-DD"),
                };
                wx.setStorageSync("signData", JSON.stringify(sign));
                this.setData({
                    sign: sign,
                });
            } else {
                Notify({ type: "danger", message: "打卡失败!" });
            }
        });
    },
    2000,
    { trailing: false }
  ),
  onClickAction(e) {
    let action = e.detail.action;
    let userSysID = app.globalData.userInfo.userSysID;
    switch (action) {
      case "otherHalf":
        //另一半
        if (userSysID === -1) {
          Notify({ type: "danger", message: "请先登录!" });
        }
        else if (this.data.otherInfo.isSingle===0) {
            wx.navigateTo({
                url: "./otherHalf/otherHalf",
              });
        }
        break;
      case "horoscope":
        //星座运势
        if (userSysID === -1) {
          Notify({ type: "danger", message: "请先登录!" });
        } else {
          wx.navigateTo({
            url: "./horoscope/horoscope",
          });
        }
        break;
      case "setting":
        //设置
        if (userSysID === -1) {
          Notify({ type: "danger", message: "请先登录!" });
        } else {
          wx.navigateTo({
            url: "./setting/setting",
          });
        }

        break;
      case "feedBack":
        //意见建议
        wx.navigateTo({
          url: "./feedBack/feedBack",
        });
        break;
      case "todayEat":
        //今天吃什么
        wx.navigateTo({
            url: "./todayEat/todayEat",
        });
        break;
      case "reward":
        //今天吃什么
        wx.navigateTo({
            url: "./reward/reward",
        });
        break;
      default:
        break;
    }
  },
  GetUserLimit(){
    let reqData = {
        LimitName: 'pay',
    }; 
    getUserLimit(reqData)
        .then(({code,data})=>{
            if(code===200){
                const {limitList=[]}=data;
                if(limitList.length>0){
                    this.setData({
                        audit:limitList[0].Enable===1,
                        title:limitList[0].Title
                    })
                }
            }
        })
  },
  GetOtherHalfInfoBySysID(userSysID){
    let reqData = {
        UserSysID: userSysID
    };
    getOtherHalfInfoBySysID(reqData)
        .then(({code,data})=>{
            if(code===200){
                const {
                    SysID=-1,UserSysID=-1,IsSign=0,ContinueSign=0,CouplesMoney=0,
                    IsSingle=0,LoveDate='',OtherHalfSysID=-1,OtherHalfNickName='',OtherHalfAvatarUrl=''
                }=data;
                if(+SysID!==-1){
                    //不是单身
                    let otherInfo = {
                        isSingle: IsSingle,
                        loveDate: LoveDate,
                        avatarUrl: OtherHalfAvatarUrl,
                        nickName: OtherHalfNickName,
                    };
                    let sign = {
                        isSign: IsSign,
                        couplesMoney: ContinueSign,
                        continueSign: CouplesMoney,
                        date: dayjs().format('YYYY-MM-DD'),
                    };
                    wx.setStorageSync("signData", JSON.stringify(sign));
                    wx.setStorage({
                        key: "otherHalfInfo",
                        data: JSON.stringify(otherInfo),
                    });
                    this.setData({
                        sign: sign,
                        otherInfo: otherInfo,
                    });
                }
            }
        })
  }
});
