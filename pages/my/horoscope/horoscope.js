const app = getApp();
import {getUserHoroscope} from '../../../api/my_v1'
Page({
    data: {
        Name:'',
        Date:'',
        Health:'',
        Love:'',
        Money:'',
        Work:'',
        IconField:'',
        BgImg:''
    },
    onShow() {
        let userSysID=app.globalData.userInfo.userSysID
        this.getUserHoroscope(userSysID)
    },
    getUserHoroscope(userSysID){
        let reqData={
            UserSysID:userSysID
        }
        getUserHoroscope(reqData)
            .then(({code,data})=>{
                if(code===200){
                    let {Name='',Date='',Health='',Love='',Money='',Work='',Icon=''}=data;
                    this.setData({
                        Name:Name,
                        Date:Date,
                        Health:Health,
                        Love:Love,
                        Money:Money,
                        Work:Work,
                        IconField:`iconfont icon-${Icon}`,
                        BgImg:'url(https://love-1300086122.file.myqcloud.com/bg/'+Icon+'.jpeg)'
                    })
                }
            })
    }
})