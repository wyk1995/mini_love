// pages/my/todayEat/components/select/select.js
Component({
    properties: {
        presetList:{
            type:Array
        },
        active:{
            type:String,
            value:'select'
        }
    },
    lifetimes:{
        attached(){
            // console.log('789')
        }
    },
    observers:{
        active:function(){
            if(this.properties.active==='select'){
                let list=this.properties.presetList.filter(item=>{
                    return item.checked
                })
                let todayEat='';
                if(list.length>0){
                    todayEat=list[0].name
                }
                this.setData({
                    list:list,
                    todayEat:todayEat,
                    btnText:'开始喽'
                })
            }
        }
    },
    data: {
        list:[],
        todayEat:'',
        btnText:'开始喽'
    },

    methods: {
        onClickBegin(){
            if(this.data.list.length!==0){
                let time=3;
                let randomInterval = setInterval(()=>{
                    let index=this.RandomNum(0,this.data.list.length-1);
                    this.setData({
                        todayEat:this.data.list[index].name
                    })
                },50)
                let timeoutInterval=setInterval(()=>{
                    time=time-1;
                    if(time<=0){
                        clearInterval(timeoutInterval)
                        clearInterval(randomInterval)
                        wx.vibrateLong()
                        this.setData({
                            btnText:'重新选择'
                        })
                    }
                },1000)
            }
        },
        //生成从minNum到maxNum的随机数
        RandomNum(minNum,maxNum){ 
            switch(arguments.length){ 
                case 1: 
                    return parseInt(Math.random()*minNum+1,10); 
                break; 
                case 2: 
                    return parseInt(Math.random()*(maxNum-minNum+1)+minNum,10); 
                break; 
                    default: 
                        return 0; 
                    break; 
            } 
        },
        onClickWatchRecipe(){

        }
    }
})
