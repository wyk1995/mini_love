// pages/loveDay/love-day-item/love-day-item.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        name:{
            type:String,
            value:''
        },
        date:{
            type:String,
            value:''
        },
        intervalDay:{
            type:Number,
            value:99999
        },
        weekly:{
            type:String,
            value:''
        },
        countDown:{
            type:Number,
            value:0
        },
        manage:{
            type:Boolean,
            value:false
        }
    },

    /**
     * 组件的初始数据
     */
    data: {

    },

    /**
     * 组件的方法列表
     */
    methods: {
        onClickDelete(){
            this.triggerEvent('click-delete')
        }
    }
})
