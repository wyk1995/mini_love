const app=getApp()
import dayjs from 'dayjs'
import {getLoveDayList,handleLoveDay} from '../../api/loveDay_v1'
import { getOtherHalfInfoBySysID } from "../../api/my_v1";
import Notify from "@vant/weapp/notify/notify";
Page({
    data: {
        app:app,
        loveDayList:[],
        otherInfo:{
            isSingle:0,
            loveDate:'',
            avatarUrl:'',
            nickName:''
        },
        intervalDay:999999,
        manage:false,
        loading:true
    },
    onLoad(){
        this.GetLoveDayList();
        let otherHalfInfo=wx.getStorageSync('otherHalfInfo')
        if(otherHalfInfo){
            let otherInfo=JSON.parse(otherHalfInfo)
            let IntervalDay=dayjs().diff(otherInfo.loveDate,'day')
            this.setData({
                otherInfo:otherInfo,
                intervalDay:IntervalDay
            })
        }
        else if(app.globalData.userInfo.userSysID!==-1){
            this.GetOtherHalfInfoBySysID(app.globalData.userInfo.userSysID);
        }
    },
    GetLoveDayList(){
        this.setData({
            loading:true
        })
        let reqData={
            UserSysID:app.globalData.userInfo.userSysID
        }
        getLoveDayList(reqData)
            .then(({code,data})=>{
                this.setData({
                    loading:false
                })
                if(code===200){
                    this.setData({
                        loveDayList:data
                    })
                }
            })
            .catch(()=>{
                this.setData({
                    loading:false
                })
            })
    },
    onClickEditLoveDay(e){
        let loveDay=e.currentTarget.dataset.item;
        wx.navigateTo({
          url: './edit-love-day/edit-love-day?loveDay='+JSON.stringify(loveDay),
        })
    },
    onClickAddLoveDay(){
        wx.navigateTo({
            url: './edit-love-day/edit-love-day'
        })
    },
    GetOtherHalfInfoBySysID(userSysID){
        let reqData = {
            UserSysID: userSysID
        };
        getOtherHalfInfoBySysID(reqData)
            .then(({code,data})=>{
                if(code===200){
                    const {
                        SysID=-1,UserSysID=-1,IsSign=0,ContinueSign=0,CouplesMoney=0,
                        IsSingle=0,LoveDate='',OtherHalfSysID=-1,OtherHalfNickName='',OtherHalfAvatarUrl=''
                    }=data;
                    if(+SysID!==-1){
                        //不是单身
                        let otherInfo = {
                            isSingle: IsSingle,
                            loveDate: LoveDate,
                            avatarUrl: OtherHalfAvatarUrl,
                            nickName: OtherHalfNickName,
                        };
                        let sign = {
                            isSign: IsSign,
                            couplesMoney: ContinueSign,
                            continueSign: CouplesMoney,
                            date: dayjs().format('YYYY-MM-DD'),
                        };
                        let IntervalDay=dayjs().diff(LoveDate,'day')
                        wx.setStorageSync("signData", JSON.stringify(sign));
                        wx.setStorage({
                            key: "otherHalfInfo",
                            data: JSON.stringify(otherInfo),
                        });
                        this.setData({
                            otherInfo: otherInfo,
                            intervalDay:IntervalDay
                        });
                    }
                }
            })
    },
    onClickManage(){
        if (app.globalData.userInfo.userSysID === -1) {
            Notify({ type: "danger", message: "请先登录!" });
        }
        else{
            this.setData({
                manage:!this.data.manage
            })
        } 
        
    },
    onClickDelete(e){
        let {SysID=-1}=e.currentTarget.dataset.item
        let reqData = {
            SysID: SysID,
            Type: "delete",
            UserSysID: app.globalData.userInfo.userSysID
        };
        handleLoveDay(reqData).then(({ code }) => {
            if (code === 200) {
                Notify({ type: "success", message: "删除成功!" });
            } else {
                Notify({ type: "danger", message: "删除失败!" });
            }
            this.GetLoveDayList()
        });
    }
})
