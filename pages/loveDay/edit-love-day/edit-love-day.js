import dayjs from "dayjs";
import calendar from "../../../static/js/calendar";
const countDownOptions=[
    {
        key: 0,
        value: "倒数",
    },
    {
        key: 1,
        value: "累积",
    },
]
const remindDateOptions = [
  {
    key: 0,
    value: "当天",
  },
  {
    key: 1,
    value: "提前一天",
  },
  {
    key: 2,
    value: "提前两天",
  },
  {
    key: 3,
    value: "提前三天",
  },
  {
    key: 4,
    value: "提前一周",
  },
  {
    key: 5,
    value: "提前一月",
  },
  {
    key: 99,
    value: "不提醒",
  },
];

const remindTimeOptions = [
  {
    key: "08：00",
    value: "08：00",
  },
  {
    key: "12：00",
    value: "12：00",
  },
  {
    key: "18：00",
    value: "18：00",
  },
];

const repeatOptions = [
  {
    key: 2,
    value: "每星期",
  },
  {
    key: 3,
    value: "每月",
  },
  {
    key: 4,
    value: "每年",
  },
  {
    key: 99,
    value: "不循环",
  },
];
const app = getApp();
import { handleLoveDay } from "../../../api/loveDay_v1";
import Notify from "@vant/weapp/notify/notify";
Page({
  data: {
    error: false,
    sysID: -1,
    name: "",
    date: {
      show: false,
      date: "",
      lunarDate: "",
      lunarOrSolar: 1,
    },
    optionType: "date",
    countDown:0,
    remind: {
      show: false,
      date: 3,
      time: "08：00",
    },
    repeat: 4,
    options: {
      show: false,
      value: 0,
      options: remindDateOptions,
    },
    text: {
      countDown:'倒数',
      date: "提前三天",
      time: "08：00",
      repeat: "每年",
    },
    loading: false,
    deleteLoading:false,
    app: app,
  },
  onLoad(options) {
    this.setData({
      app: app,
    });
    let { loveDay = "" } = options;
    if (loveDay === "") {
      //新增
      wx.setNavigationBarTitle({
        title: "新增纪念日",
      });
      this.setData({
        "date.date": dayjs().format("YYYY-MM-DD"),
      });
    } else {
      //编辑
      wx.setNavigationBarTitle({
        title: "编辑纪念日",
      });
      let {
        CommemorationName,
        CommemorationDate,
        LunarDate,
        LunarOrSolar,
        RemindDate,
        RemindTime,
        SysID,
        Circulation,
        CountDown
      } = JSON.parse(loveDay);
      let newDate = {
        show: false,
        date: CommemorationDate,
        lunarDate: LunarDate,
        lunarOrSolar: LunarOrSolar,
      };
      let remind = {
        show: false,
        date: RemindDate,
        time: RemindTime,
      };
      let text = {
        date: this.computeText("date", RemindDate),
        time: this.computeText("time", RemindTime),
        repeat: this.computeText("repeat", Circulation),
        countDown: this.computeText("countDown", CountDown),
      };
      this.setData({
        sysID: SysID,
        name: CommemorationName,
        date: newDate,
        remind: remind,
        repeat: Circulation,
        countDown:CountDown,
        text: text,
      });
    }
  },
  onClickSave() {
    if (this.data.name === "") {
      this.setData({
        error: true,
      });
    } else {
      this.setData({
        loading: true,
      });
      let reqData = {
        Type: this.data.sysID === -1 ? "add" : "edit",
        SysID: this.data.sysID,
        UserSysID: app.globalData.userInfo.userSysID,
        CommemorationName: this.data.name,
        BaseCommemorationDate: dayjs(this.data.date.date).format("YYYY-MM-DD"),
        LunarOrSolar: this.data.date.lunarOrSolar,
        RemindDate: this.data.remind.date,
        RemindTime: this.data.remind.time,
        Circulation: this.data.repeat,
        CountDown:this.data.countDown
      };
      handleLoveDay(reqData).then(({ code }) => {
        this.setData({
          loading: false,
        }); 
        if (code === 200) {
          let pages = getCurrentPages();
          let prevPage = pages[pages.length - 2]; 
          prevPage.GetLoveDayList();
          wx.navigateBack({
            delta: 1,
          });
        } else {
          Notify({ type: "danger", message: "保存失败!" });
        }
      });
    }
  },
  GetDateWeekly(date) {
    let weekly = "周一";
    let key = dayjs(date).day();
    switch (key) {
      case 0:
        weekly = "周日";
        break;
      case 1:
        weekly = "周一";
        break;
      case 2:
        weekly = "周二";
        break;
      case 3:
        weekly = "周三";
        break;
      case 4:
        weekly = "周四";
        break;
      case 5:
        weekly = "周五";
        break;
      case 6:
        weekly = "周六";
        break;
    }
    return weekly;
  },
  onChangeValue(e) {
    let key = e.currentTarget.dataset.key;
    switch (key) {
      case "countDown":
        this.data.optionType = "countDown";
        let newCountDownOptions = {
          show: true,
          value: this.data.countDown,
          options: countDownOptions,
        };
        this.setData({
          options:newCountDownOptions,
        });
        break;
      case "date":
        this.setData({
          "date.show": true,
        });
        break;
      case "remind":
        this.setData({
          "remind.show": true,
        });
        break;
      case "repeat":
        this.data.optionType = "repeat";
        let newRepeatOptions = {
          show: true,
          value: this.data.repeat,
          options: repeatOptions,
        };
        this.setData({
          options:newRepeatOptions,
        });
        break;
      default:
        break;
    }
  },
  onCloseRemind(e) {
    this.setData({
      "remind.show": false,
    });
  },
  onClickRemind(e) {
    let key = e.currentTarget.dataset.key;
    this.data.optionType = key;
    let options = {};
    switch (key) {
      case "date":
        options = {
          show: true,
          value: this.data.remind.date,
          options: remindDateOptions,
        };
        this.setData({
          options,
        });
        break;
      case "time":
        options = {
          show: true,
          value: this.data.remind.time,
          options: remindTimeOptions,
        };
        this.setData({
          options,
        });
        break;
      default:
        break;
    }
  },
  onCloseOption() {
    this.setData({
      "options.show": false,
    });
  },
  onChangeRadio(e) {
    let key = e.detail;
    switch (this.data.optionType) {
        case "countDown":
            this.setData({
                countDown: key,
                "text.countDown": this.computeText(this.data.optionType, key),
            });
        break;
      case "date":
        this.setData({
          "remind.date": key,
          "text.date": this.computeText(this.data.optionType, key),
        });
        break;
      case "time":
        this.setData({
          "remind.time": key,
          "text.time": this.computeText(this.data.optionType, key),
        });
        break;
      case "repeat":
        this.setData({
          repeat: key,
          "text.repeat": this.computeText(this.data.optionType, key),
        });
        break;

      default:
        break;
    }
  },
  computeText(key, value) {
    let text = "";
    let index = -1;
    switch (key) {
        case "countDown":
            index = countDownOptions.findIndex((item) => item.key === value);
            if (index !== -1) {
                text = countDownOptions[index].value;
            }
        break;
      case "date":
        index = remindDateOptions.findIndex((item) => item.key === value);
        if (index !== -1) {
          text = remindDateOptions[index].value;
        }
        break;
      case "time":
        index = remindTimeOptions.findIndex((item) => item.key === value);
        if (index !== -1) {
          text = remindTimeOptions[index].value;
        }
        break;
      case "repeat":
        index = repeatOptions.findIndex((item) => item.key === value);
        if (index !== -1) {
          text = repeatOptions[index].value;
        }
        break;
      default:
        break;
    }
    return text;
  },
  onChangeLunar(e) {
    this.setData({
      "date.lunarOrSolar": e.detail.lunar ? 0 : 1,
    });
  },
  onConfirmDate(e) {
    let dayInfo = e.detail.dayInfo;
    let loveDate = dayInfo.date;
    let { date, lunarDate, IMonthCn, IDayCn, astro } = calendar.solar2lunar(
      dayjs(loveDate).format("YYYY"),
      dayjs(loveDate).format("MM"),
      dayjs(loveDate).format("DD")
    );
    let newDate = {
      show: false,
      date: dayjs(date).format("YYYY-MM-DD"),
      lunarDate: IMonthCn + IDayCn,
      lunarOrSolar: this.data.date.lunarOrSolar,
    };
    this.setData({
      date: newDate,
    });
  },
  onHideDate() {
    this.setData({
      "date.show": false,
    });
  },
  onChangeName(e) {
    this.setData({
      name: e.detail.trim(),
      error: false,
    });
  },
  onBlurName(e) {
    let value = e.detail.value.trim();
    if ((value ?? "") === "") {
      this.setData({
        error: true,
      });
    }
  },
  onClickDelete(){
    let reqData = {
        SysID: this.data.sysID,
        Type: "delete",
        UserSysID: app.globalData.userInfo.userSysID
    };
    this.setData({
        deleteLoading: true,
    });
    handleLoveDay(reqData).then(({ code }) => {
        this.setData({
            deleteLoading: false,
        });
        if (code === 200) {
            let pages = getCurrentPages();
            let prevPage = pages[pages.length - 2]; 
            prevPage.GetLoveDayList();
            wx.navigateBack({
                delta: 1,
            });
        } else {
            Notify({ type: "danger", message: "保存失败!" });
        }
    });
  }
});
