import dayjs from "dayjs";
const app = getApp();

import {completeUserLittleThings,getCompletedUserLittleThingsInfo} from "../../../api/list_v1";

import Notify from "@vant/weapp/notify/notify";
Page({
  data: {
    app: app,
    sysID: "",
    thingName: "",
    completeDate: "",
    address: "",
    thingContent: "",
    fileList: [],
    isCompleted: 0,
    loading: false,
    error: {
      error_date: false,
      error_date_message: "请选择完成日期",
      error_address: false,
      error_address_message: "请输入完成地点",
    },
    dateShow: false,
  },
  onLoad(options) {
    let { littleThing } = options;
    let { SysID, ThingName, ThingContent, IsCompleted } =
      JSON.parse(littleThing);
    if (IsCompleted === 1) {
      //已完成
      this.data.sysID = SysID;
      this.GetCompletedUserLittleThingsInfo();
    } else {
      this.setData({
        sysID: SysID,
        thingName: ThingName,
        completeDate: dayjs().format("YYYY-MM-DD"),
        thingContent: ThingContent,
      });
    }
  },
  onClickDate() {
    if (this.data.isCompleted !== 1) {
      this.setData({
        dateShow: true,
      });
    }
  },
  onConfirmDate(e) {
    let dayInfo = e.detail.dayInfo;
    this.setData({
      dateShow: false,
      completeDate: dayInfo.date,
    });
  },
  onHideDate() {
    this.setData({
      dateShow: false,
    });
  },
  onChangeAddress(e) {
    this.setData({
      address: e.detail.trim(),
      "error.error_address": false,
    });
  },
  onBlurAddress(e) {
    if (e.detail.value.trim() === "") {
      this.setData({
        "error.error_address": true,
      });
    }
  },
  afterRead(data) {
    let file = data.detail.file;
    let fileList = this.data.fileList;
    file.forEach((item) => {
      if (fileList.length < 10) {
        fileList.push(item);
      }
    });
    this.setData({
      fileList: fileList,
    });
  },
  async onClickSave() {
    if (this.data.address === "") {
      this.setData({
        "error.error_address": true,
      });
    } else {
      let fileList = await this.GetImageList();
      let data = {
        SysID: this.data.sysID,
        CompleteTime: dayjs(this.data.completeDate).format("YYYY-MM-DD"),
        CompleteAddress: this.data.address,
        ThingContent: this.data.thingContent,
        ImageData: fileList,
      };
      this.setData({
        loading: true,
      });
      completeUserLittleThings(data)
        .then(({ code }) => {
            this.setData({
                loading: false,
            });
            if (code === 200) {
                let pages = getCurrentPages();
                let prevPage = pages[pages.length - 2]; 
                prevPage.GetUserLittleThings();
                this.GetCompletedUserLittleThingsInfo();
            } else {
                Notify({ type: "danger", message: "保存失败!" });
            }
        })
        .catch(e=>{
            this.setData({
                loading: false,
            });
            Notify({ type: "danger", message: "保存失败!" });
        })
    }
  },
  async GetImageList() {
    let filePromise = this.data.fileList.map(async (item) => {
      const response = await this.UploadImgToBase64(item.url);
      let { data } = response;
      return data;
    });
    let fileList = [];
    for (let textPromise of filePromise) {
      fileList.push(await textPromise);
    }
    return fileList;
  },
  UploadImgToBase64(file) {
    return new Promise((resolve, reject) => {
      wx.getFileSystemManager().readFile({
        filePath: file,
        encoding: "base64",
        success: (res) => {
          return resolve(res);
        },
        fail(res) {
          return reject(res);
        },
      });
    });
  },
  GetCompletedUserLittleThingsInfo(){
    let reqData = {
        SysID: this.data.sysID,
    };
    getCompletedUserLittleThingsInfo(reqData).then(({ code, data }) => {
        if (code === 200) {
            let {
                ThingName,
                CompleteTime,
                ThingContent,
                CompleteAddress,
                ImageData=[],
            } = data;
            this.setData({
                thingName: ThingName,
                completeDate: CompleteTime,
                thingContent: ThingContent,
                address: CompleteAddress,
                fileList: ImageData,
                isCompleted: 1,
            });
        }
    });
  }
});
