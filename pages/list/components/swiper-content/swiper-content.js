Component({
    properties: {
        littleThings:{
            type:Array,
            value:[]
        }
    },
    data: {
        current:0
    },
    methods: {
        onChangeCurrent(e){
            let currentIndex=e.detail.current
            this.setData({
                current:currentIndex
            })
        },
        onClickSwiperItem(e){
            let littleThing=e.currentTarget.dataset.thing;
            wx.navigateTo({
              url: './complete-little-thing/complete-little-thing?littleThing='+JSON.stringify(littleThing),
            })
        }
    }
})
