const app=getApp()
import {getUserLittleThings} from '../../api/list_v1'
Page({
    data: {
        app:app,
        menu_option: [],
        menuChecked: 'all',
        menuText:'全部小事',
        menuShow:false,
        littleThings:[],
        type:'list'
    },
    onLoad(){
        this.GetUserLittleThings()
    },
    onChangeType(){
        this.setData({
            menuShow:false,
            type:this.data.type==='list'?'swiper':'list'
        })
    },
    GetUserLittleThings(){
        let reqData={
            UserSysID:app.globalData.userInfo.userSysID,
            ThingsType:this.data.menuChecked
        }
        getUserLittleThings(reqData)
            .then(({code,data:resData})=>{
                if(code===200){
                    const {thingsList=[],thingsCount={}}=resData
                    const {AllCount=0,Complete=0,UnComplete=0}=thingsCount
                    
                    let menu_option=[
                        { text: `全部小事（${Complete}/${AllCount}）`, value: 'all' },
                        { text: `只看已完成（${Complete}）`, value: 'complete' },
                        { text: `只看未完成（${UnComplete}）`, value: 'unComplete' },
                    ]
                    this.setData({
                        menu_option:menu_option,
                        littleThings:thingsList,
                        menuText:this.GetMenuText(menu_option,this.data.menuChecked)
                    })
                }
            })
    },
    GetMenuText(menu_option,menuChecked){
        let menu_text='';
        let index=menu_option.findIndex(item=>item.value===menuChecked)
        if(index!==-1){
            menu_text=menu_option[index].text
        }
        return menu_text
    },
    onShowMenu(){
        this.setData({
            menuShow:!this.data.menuShow
        })
    },
    onChangeOption(e){
        let key=e.currentTarget.dataset.key;
        this.data.menuChecked=key;
        this.setData({
            menuShow:false,
            menuChecked:key
        })
        this.GetUserLittleThings()
    },
    onClickOverlay(){
        this.setData({
            menuShow:false
        })
    }
})
