// pages/letter/components/letter-item/letter-item.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        letterUrl:{
            type:String,
            value:''
        },
        toName:{
            type:String,
            value:''
        },
        fromName:{
            type:String,
            value:''
        }
    },

    /**
     * 组件的初始数据
     */
    data: {

    },

    /**
     * 组件的方法列表
     */
    methods: {
        onClickDetail(){
            this.triggerEvent('click-detail')
        },
        onClickPreviewImage(e){
            const url=e.currentTarget.dataset.url;
            console.log(url)
            if(url!==''){
                console.log('789')
                wx.previewImage({
                    current: url, 
                    urls: [url] 
                })
            }
        }
    }
})
