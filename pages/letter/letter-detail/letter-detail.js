const app=getApp()
import {getLetterDetail} from '../../../api/letter_v1'
Page({
    onLoad(query){
        const {sysID=''}=query
        if (sysID===null||sysID==undefined||sysID==='') {
            const param=decodeURIComponent(query.scene);
            if(param!=null&&param!=undefined&&param!=''){
                let paramArray=param.split('=');
                if(paramArray.length>0){
                    this.data.sysID=paramArray[1];
                }
            }
        }
        else{
            this.data.sysID=sysID
        }
    },
    onShow(){
        let reqData={
            SysID:this.data.sysID,
            UserSysID:app.globalData.userInfo.userSysID
        }
        this.GetLetterDetail(reqData);
    },
    data: {
        app:app,
        sysID:'',
        toName:'',
        fromName:'',
        content:'',
        showAll:false,
        userBox_height: '',
        animationData: {}
    },
    onClick(){
      this.setData({
        showAll:true
      })
    },
    GetLetterDetail(reqData){
        getLetterDetail(reqData)
            .then(({code,data,msg})=>{
                if (code===200) {
                    const {SysID='',ToName='',FromName='',Content=''}=data;
                    if(SysID!==''){
                        this.setData({
                            toName:ToName,
                            fromName:FromName,
                            content:Content.replace(/<img/gi, '<img style="width:90%; height:auto"'),
                            showAll:false,
                        })
                    }
                }
                else{

                }
            })
    }
})