import {axios} from '../utils/http1'

export const getUserOpenID=(reqData)=>{
    return axios.get('/v1/wechat/user/getUserOpenID',reqData)
}

export const uploadAvatar=(reqData)=>{
    return axios.post('/v1/wechat/user/uploadAvatar',reqData)
}

export const sendRegisterSmsCode=(reqData)=>{
    return axios.post('/v1/wechat/user/sendRegisterSmsCode',reqData)
}

export const registerUser=(reqData)=>{
    return axios.post('/v1/wechat/user/registerUser',reqData)
}

export const userSignIn=(reqData)=>{
    return axios.post('/v1/wechat/user/userSignIn',reqData)
}

export const getOtherHalfInfoBySysID=(reqData)=>{
    return axios.get('/v1/wechat/user/getOtherHalfInfoBySysID',reqData)
}

export const getUserLimit=(reqData)=>{
    return axios.get('/v1/wechat/user/getUserLimit',reqData)
}

export const getUserHoroscope=(reqData)=>{
    return axios.get('/v1/wechat/user/getUserHoroscope',reqData)
}

export const submitUserFeedBack=(reqData)=>{
    return axios.post('/v1/wechat/user/submitUserFeedBack',reqData)
}

export const getUserLoveCode=(reqData)=>{
    return axios.get('/v1/wechat/user/getUserLoveCode',reqData)
}

export const getUserInfoByLoveCode=(reqData)=>{
    return axios.get('/v1/wechat/user/getUserInfoByLoveCode',reqData);
}

export const bindUserOtherHalf=(reqData)=>{
    return axios.post('/v1/wechat/user/bindUserOtherHalf',reqData);
}

export const handleUser=(reqData)=>{
    return axios.post('/v1/wechat/user/handleUser',reqData);
}

export const getUserInfoBySysID=(reqData)=>{
    return axios.get('/v1/wechat/user/getUserInfoBySysID',reqData);
}