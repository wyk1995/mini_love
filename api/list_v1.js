import {axios} from '../utils/http1'

export const getUserLittleThings=(reqData)=>{
    return axios.get('/v1/wechat/little-things/getUserLittleThings',reqData)
}

export const completeUserLittleThings=(reqData)=>{
    return axios.post('/v1/wechat/little-things/completeUserLittleThings',reqData)
}

export const getCompletedUserLittleThingsInfo=(reqData)=>{
    return axios.get('/v1/wechat/little-things/getCompletedUserLittleThingsInfo',reqData)
}