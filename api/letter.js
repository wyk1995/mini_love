import {axios} from '../utils/http'

export const getLetterCode=(data)=>{
    return axios.post('v1/letter/GetLetterCode',data)
}

export const getLetterDetail=(data)=>{
    return axios.get('v1/letter/GetLetterDetail',data)
}

export const insertLetterImage=(data)=>{
    return axios.post('v1/letter/InsertLetterImage',data)
}