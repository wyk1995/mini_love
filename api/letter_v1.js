import {axios} from '../utils/http1'

export const getUserLetterList=(reqData)=>{
    return axios.get('/v1/wechat/letter/getUserLetterList',reqData)
}

export const getLetterDetail=(reqData)=>{
    return axios.get('/v1/wechat/letter/getLetterDetail',reqData)
}

export const insertLetterImage=(reqData)=>{
    return axios.post('/v1/wechat/letter/insertLetterImage',reqData)
}

export const insertLetter=(reqData)=>{
    return axios.post('/v1/wechat/letter/insertLetter',reqData)
}

