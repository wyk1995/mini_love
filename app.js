// app.js
App({
    onLaunch() {
        const that = this;
        // 获取系统信息
        const systemInfo = wx.getSystemInfoSync();
        // 胶囊按钮位置信息
        const menuButtonInfo = wx.getMenuButtonBoundingClientRect();
        // 导航栏高度 = 状态栏高度 + 44
        that.globalData.navInfo.navBarHeight = systemInfo.statusBarHeight + 44;
        that.globalData.navInfo.menuRight = systemInfo.screenWidth - menuButtonInfo.right;
        that.globalData.navInfo.menuBotton = menuButtonInfo.top - systemInfo.statusBarHeight;
        that.globalData.navInfo.menuHeight = menuButtonInfo.height;
    },
    globalData: {
        userInfo: wx.getStorageSync('userInfo')?JSON.parse(wx.getStorageSync('userInfo')): {
            userSysID:-1,
            nickName:'',
            avatarUrl:''
        },
        openId:wx.getStorageSync('openId')?wx.getStorageSync('openId'):'',
        navInfo:{
            navBarHeight: 0, // 导航栏高度
            menuRight: 0, // 胶囊距右方间距（方保持左、右间距一致）
            menuBotton: 0, // 胶囊距底部间距（保持底部间距一致）
            menuHeight: 0, // 胶囊高度（自定义内容可与胶囊高度保证一致）
        }
    }
})
